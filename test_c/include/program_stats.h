#ifndef PROGRAM_STATS_H
#define PROGRAM_STATS_H

#include <stdint.h>

typedef struct ProgramStats ProgramStats;

ProgramStats* ProgramStats_new(void);
void ProgramStats_destroy(ProgramStats* program_stats);


void ProgramStats_start_timer(ProgramStats* program_stats);
void ProgramStats_end_timer(ProgramStats* program_stats);
void ProgramStats_get_executable_size(ProgramStats* program_stats, const char* file_path); 
#endif