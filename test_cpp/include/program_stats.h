#ifndef PROGRAM_STATS_H
#define PROGRAM_STATS_H

#include <chrono>
#include <filesystem>

/// @brief Class to provide statistics about the program
class ProgramStats
{
    public:
        explicit ProgramStats(void);
        ~ProgramStats();

        // Entry point for the timer
        void start_timer(void);

        // Exit point for the timer, prints the time taken
        void end_timer(void);

        // Prints the size of the executable
        uint64_t get_executable_size(const char* file_path);

    private:

        void _print_time_taken(void);

        std::chrono::time_point<std::chrono::high_resolution_clock> _start_timer;
        std::chrono::time_point<std::chrono::high_resolution_clock> _end_timer;
        std::chrono::duration<float, std::milli> _elapsed_time;
};

#endif 