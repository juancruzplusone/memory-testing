#include "program_stats.h"
#include <iostream>

ProgramStats::ProgramStats(void)
{
}

ProgramStats::~ProgramStats(void)
{
}

void ProgramStats::start_timer(void)
{
    _start_timer = std::chrono::high_resolution_clock::now();
}

void ProgramStats::end_timer(void)
{
    _end_timer = std::chrono::high_resolution_clock::now();
    _elapsed_time = _end_timer - _start_timer;

    std::cout << "Time taken: " << _elapsed_time.count() << "ms" << std::endl;
}

uint64_t ProgramStats::get_executable_size(const char* file_path)
{
    std::filesystem::path path(file_path);
    uint64_t file_size = std::filesystem::file_size(path); 
    
    return file_size;
}