#include "program_stats.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// Actual structure definition - now hidden from other files
struct ProgramStats 
{
    struct timespec _start_time;
    struct timespec _end_time;
    long _elapsed_time;
};

ProgramStats* ProgramStats_new(void) 
{
    ProgramStats* stats = calloc(1, sizeof(ProgramStats));
    return stats;
}

void ProgramStats_destroy(ProgramStats* stats) 
{
    if (stats) 
    {
        free(stats);
    }
}

void ProgramStats_start_timer(ProgramStats* stats) 
{
    clock_gettime(CLOCK_MONOTONIC, &stats->_start_time);  // Save start time in _start_time
}

void ProgramStats_end_timer(ProgramStats* stats) 
{
    clock_gettime(CLOCK_MONOTONIC, &stats->_end_time);  // Save end time in _end_time

    // Calculate elapsed time considering both seconds and nanoseconds
    stats->_elapsed_time = (stats->_end_time.tv_sec - stats->_start_time.tv_sec) * 1000;     
    stats->_elapsed_time += (stats->_end_time.tv_nsec - stats->_start_time.tv_nsec) / 1000000;  
    printf("Time taken: %ld ms\n", stats->_elapsed_time);
}

void ProgramStats_get_executable_size(ProgramStats* stats, const char* file_path)
{
    stats = stats;

    FILE* file = fopen(file_path, "r");
    // Check if file exists
    if (file) 
    {
        // Move file pointer to end
        fseek(file, 0, SEEK_END);

        // return the current position of the file pointer, which is amount of bytes traversed
        uint64_t size = ftell(file);

        // Close the file
        fclose(file);

        uint64_t size_kb = size / 1000;
        printf("Executable size: %llu Kb\n", size_kb);
    }
}
