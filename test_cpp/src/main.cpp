#include <iostream>
#include <cassert>
#include "program_stats.h"

int main(int argc, char *argv[]) {
    // Ensure that the user has provided something to print
    assert(argc == 2);

    ProgramStats stats;
    uint64_t exe_size = stats.get_executable_size(argv[0]);
    std::cout << "Executable size: " << exe_size << " bytes" << std::endl;

    stats.start_timer();

    char *p = argv[1];
    std::cout << "You said: " << p << std::endl;
    stats.end_timer();
}