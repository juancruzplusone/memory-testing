#include <assert.h>
#include <stdio.h>
#include "program_stats.h"

int main(int argc, char* argv[]) {
    // Ensure that one argument is provided
    assert(argc == 2);


    // Create a new ProgramStats instance
    ProgramStats* stats = ProgramStats_new();
    if (!stats) 
    {
        printf("Failed to create stats object.\n");
        return 1;
    }

    // Get the executable size
    ProgramStats_get_executable_size(stats, argv[0]);


    ProgramStats_start_timer(stats);

    // Perform operations
    printf("You entered: %s\n", argv[1]);

    ProgramStats_end_timer(stats);

    // Destroy the ProgramStats instance to free memory
    ProgramStats_destroy(stats);

    return 0;
}