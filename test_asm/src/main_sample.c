#include <assert.h>
#include <stdio.h>

int main(int argc, char* argv[]) 
{
    assert(argc == 2);
    printf("You said: %s\n", argv[1]);
    return 0;
}